const locations = [
    {
        name: "Central Park, NY",
        lat: 40.785091,
        lng: -73.968285

    }, {
        name: "Lisbon",
        lat: 38.722252,
        lng: -9.139337

    }
    , {
        name: "Madrid",
        lat: 40.416775,
        lng: -3.70379

    }, {
        name: "Zanzibar",
        lat: -6.159,
        lng: 39.1926

    }, {
        name: "College Of Management",
        lat: 31.97035,
        lng: 34.772104

    }, {
        name: "Haifa",
        lat: 32.583331,
        lng: 35.0

    }, {
        name: "Wadi Araba Crossing",
        lat: 29.57166438,
        lng: 34.973496106

    }
]

module.exports = locations;
